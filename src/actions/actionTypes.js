export const ADD_POKEMON_BY_TYPE = 'ADD_POKEMON_BY_TYPE';
export const FILTER_POKEMON_BY_TYPE = 'FILTER_POKEMON_BY_TYPE';
export const ADD_POKEMON_BY_ABILITY = 'ADD_POKEMON_BY_ABILITY';
export const FILTER_POKEMON_BY_ABILITY = 'FILTER_POKEMON_BY_ABILITY';